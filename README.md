# Simple Forum App
### Web application for \<epam> UI trainings in the Debrecen office.

#### Prerequisites
* [NodeJS (8.6.0)](https://nodejs.org/en/)
* NPM
* [Gulp](https://gulpjs.com/)
* [Yarn](https://yarnpkg.com/en/)

#### Install Dependencies
```
yarn install
```

#### Build
```
gulp build
```

### Run
```
gulp
```
