/**
 * @author  attilagyongyosi
 * @since   0.0.3
 *
 * @description
 * This module is the entry point of the Forum app server.
 */
(function (config) {
    'use strict';

    const logger = require('./src/server/logger/logger');

    const express       = require('express');
    const bodyParser    = require('body-parser');
    const topicApi      = require('./src/server/topic/topic.api');
    const messageApi    = require('./src/server/message/message.api');

    const port          = config.get('server.port');

    let app = express();

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(express.static(config.get('paths.ui')));

    app.use('/api', topicApi);
    app.use('/api', messageApi);

    process.on('db:connected', () => {
        app.listen(port, () => {
            logger.info('Server started on port %d successfully.', port);
        });
    });

    process.on('db:error', () => {
        app.close();
    });

}(require('config')));
