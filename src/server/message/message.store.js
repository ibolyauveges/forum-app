/**
 * @author  attilagyongyosi
 * @since   0.0.3
 *
 * @description
 * This module provides operations for manipulating messages on
 * a specific topic.
 * The underlying technology is NeDB. Database configuration can
 * be found in config/default.js.
 *
 * @see     database.js
 * @see     message.model.js
 * @see     message.api.js
 */
(function () {
    'use strict';

    const log           = require('../logger/logger');
    const database      = require('../database/database');
    let Message         = require('./message.model');

    const messagesProjection = { messages: 1, _id: 0 };

    let id = 0;
    let operations = {};

    operations.addMessage = function (topicId, message, onSuccess, onError) {
<<<<<<< HEAD
        if (!topicId || typeof topicId !== 'number')    { onError('Unknown topic ID!'); }
        if (!message || !(message instanceof Message))  { onError('Data is not a message!'); }
=======
        if (!topicId || typeof topicId !== 'number')    { onError('Unknown topic ID!'); return; }
        if (!message || !(message instanceof Message))  { onError('Data is not a message!'); return; }
>>>>>>> 656eb60b465a4ffeb7dbcba629b58a05356034d8

        log.debug('Saving new message for topic %d...', topicId);

        message._id = message._id || id;
        database.update({ _id: topicId }, { $addToSet: { messages: message }}, (error) => {
            if (error) {
                onError(error);
                return;
            }

            log.debug('New message saved successfully for topic %d.', topicId);
            id++;
            onSuccess(message);
        });
    };

    operations.getAll = function (topicId, onSuccess, onError) {
<<<<<<< HEAD
        if (!topicId || typeof topicId !== 'number')    { onError('Unknown topic ID!'); }
=======
        if (!topicId || typeof topicId !== 'number')    { onError('Unknown topic ID!'); return; }
>>>>>>> 656eb60b465a4ffeb7dbcba629b58a05356034d8
        log.debug('Retrieving messages for topic %d...', topicId);

        database.findOne({ _id: topicId }, messagesProjection, (error, docs) => {
            if (error) {
                onError(error);
                return;
            }

<<<<<<< HEAD
            let messages = [];
=======
            let messages = docs.messages || [];
>>>>>>> 656eb60b465a4ffeb7dbcba629b58a05356034d8
            log.debug('Found %d messages on topic %d.', messages.length, topicId);
            onSuccess(messages);
        });
    };

    operations.findByTitle = function (topicId, searchTerm, onSuccess, onError) {
<<<<<<< HEAD
        if (!topicId || typeof topicId !== 'number')    { onError('Unknown topic ID!'); }
=======
        if (!topicId || typeof topicId !== 'number')    { onError('Unknown topic ID!'); return; }
>>>>>>> 656eb60b465a4ffeb7dbcba629b58a05356034d8
        searchTerm = searchTerm || '';

        log.debug('Finding messages that contain the text %s', searchTerm);
        database.findOne({ _id: topicId }, messagesProjection, (error, docs) => {
            if (error) {
                onError(error);
                return;
            }

            let messages = docs.messages || [];
            messages = messages.messages.filter((message) => {
                return message.text.toLowerCase().match(new RegExp(searchTerm.toLowerCase()));
            });

            log.debug('Found %d messages that match the search criteria %s.', messages.length, searchTerm);
            onSuccess(messages);
        });
    };

    module.exports = operations;

}());
