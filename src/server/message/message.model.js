/**
 * @author  attilagyongyosi
 * @since   0.0.3
 *
 * @description
 * This module represents a message object.
 *
 * @see     message.store.js
 * @see     message.api.js
 */
(function() {
    'use strict';

    const sanitizer = require('../sanitizer/sanitizer');

    module.exports = function (email, text, creationDate) {
        this.email          = sanitizer.sanitizeString(email);
        this.text           = sanitizer.sanitizeString(text);
        this.creationDate   = creationDate || new Date();
    };
}());
