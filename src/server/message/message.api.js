/**
 * @author  attilagyongyosi
 * @since   0.0.3
 *
 * @description
 * This module creates a dedicated router for the Topic Messages API.
 * It provides REST-ish HTTP endpoints for various message-related
 * operations.
 *
 * @see     message.store.js
 * @see     message.model.js
 */
(function () {
    'use strict';

    const log = require('../logger/logger');

    const express   = require('express');
    const database  = require('./message.store');
    let Message     = require('./message.model');

    const apiRoot = '/topics/:id/messages';
    let apiRouter = express.Router();

    let handlers = {
        create: (request, response) => {
            let message = request.body;
            let topicId = parseInt(request.params.id, 10);

            if (!message) {
                response.sendStatus(400);
                return;
            }

            database.addMessage(topicId, new Message(message.email, message.text), (message) => {
                response.status(201).json(message._id);
            }, (error) => {
                response.status(500).send(error);
            });
        },
        getAll: (request, response) => {
            let topicId = parseInt(request.params.id, 10);
            let searchTerm = request.query.searchString;

            if (searchTerm) {
                database.findByTitle(topicId, searchTerm, (messages) => {
                    response.status(200).json(messages);
                }, (error) => {
                    response.status(500).send(error);
                });
            } else {
                database.getAll(topicId, (messages) => {
                    response.status(200).json(messages);
                }, (error) => {
                    response.status(500).send(error);
                });
            }
        }
    };

    apiRouter.post(apiRoot, handlers.create);
    apiRouter.get(apiRoot, handlers.getAll);

    log.info('Messages API initialized.');
    module.exports = apiRouter;

}());
