/**
 * @author  attilagyongyosi
 * @since   0.0.3
 *
 * @description
 * This module initializes a NeDB database for topics and their messages.
 * When database initialization fails, it will emit an event which will
 * stop the NodeJS server.
 *
 * @see     topic.store.js
 * @see     message.store.js
 */
(function (config) {
    'use strict';

    const log   = require('../logger/logger');
    const Store = require('nedb');

    let dbConfig = config.get('database');

    dbConfig.onload = function (error) {
        if (error) {
            log.error('Could not load database, stopping application!', error);
            process.emit('db:error');
        } else {
            log.info('Database has been initialized successfully.');
            process.emit('db:connected');
        }
    };

    module.exports =  new Store(dbConfig);

}(require('config')));
