/**
 * @author  attilagyongyosi
 * @since   0.0.3
 *
 * @description
 * This module provides data manipulation operations
 * on Topic objects.
 * The underlying store is a persistent NeDB database.
 * Configuration can be found in config/default.js.
 *
 * @see     topic.model.js
 * @see     topic.api.js
 */
(function () {
    'use strict';

    const log       = require('../logger/logger');
    const database  = require('../database/database');
    let Topic       = require('./topic.model');

    let id = 0;
    let operations = {};

    operations.addTopic = function (topic, onSuccess, onError) {
        log.debug('Saving new topic...');
        if (!(topic instanceof Topic)) {
            throw new Error('Invalid topic to insert!');
        }

        if (!topic._id) { topic._id = id; }
        database.insert(topic, (error, newTopic) => {
            if (error) {
                onError(error);
                return;
            }

            id++;
            log.debug('New topic has been successfully saved with id %d.', topic._id);
            onSuccess(newTopic);
        });
    };

    operations.findAll = function (onSuccess, onError) {
        log.debug('Retrieving every topic...');
        database.find({}, (error, topics) => {
            if (error) {
                onError(error);
                return;
            }

            log.debug('Found %d topics.', topics.length);
            onSuccess(topics);
        });
    };

    operations.findById = function (id, onSuccess, onError) {
        log.debug('Getting topic with ID %d...', id);
        database.findOne({ _id: id }, (error, topic) => {
            if (error || !topic) {
                onError(error);
                return;
            }

            onSuccess(topic);
        });
    };

    operations.findInTitle = function (titlePart, onSuccess, onError) {
        log.debug('Finding topics with "%s" in the title...', titlePart);
        database.find({ title: new RegExp(titlePart, 'g') }, (error, topics) => {
            if (error) {
                onError(error);
                return;
            }

            log.debug('Found %d topics matching the search term in their title.', topics.length);
            onSuccess(topics);
        });
    };

    module.exports = operations;

}());
