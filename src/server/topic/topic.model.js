/**
 * @author  attilagyongyosi
 * @since   0.0.3
 *
 * @description
 * This module exposes a Topic prototype.
 *
 * @see     topic.store.js
 * @see     topic.api.js
 */
(function () {
    'use strict';

    const sanitizer = require('../sanitizer/sanitizer');

    function sanitizeItem(item) {
        return {
            _id:            item._id,
            email:          sanitizer.sanitizeString(item.email),
            text:           sanitizer.sanitizeString(item.text),
            creationDate:   item.creationDate
        };
    }

    function sanitizeItems(items) {
        if (!items) { return []; }
        return items.map((item) => {
            return sanitizeItem(item);
        });
    }

    module.exports = function (title, email, creationDate, messages) {
        this.title          = sanitizer.sanitizeString(title);
        this.email          = sanitizer.sanitizeString(email);
        this.creationDate   = creationDate || new Date();
        this.messages       = sanitizeItems(messages) || [];
    };

}());
