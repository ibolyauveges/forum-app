/**
 * @author  attilagyongyosi
 * @since   0.0.3
 *
 * @description
 * This module defines REST-ish HTTP endpoints for
 * manipulating topics in the Forum Application.
 *
 * @see     topic.store.js
 * @see     topic.model.js
 */
(function () {
    'use strict';

    const log = require('../logger/logger');

    const apiRoot = '/topics';

    const express   = require('express');
    let Topic       = require('./topic.model');
    const database  = require('./topic.store');

    let apiRouter = express.Router();

    let handlers = {
        all: (request, response) => {
            let searchTerm = request.query.searchString;

            if (searchTerm) {
                database.findInTitle(searchTerm, (topics) => {
                    response.status(200).json(topics);
                }, (error) => {
                    response.status(500).send(error);
                });
            } else {
                database.findAll((topics) => {
                    response.status(200).json(topics);
                }, (error) => {
                    response.status(500).send(error);
                });
            }
        },
        one: (request, response) => {
            let id = parseInt(request.params.id, 10);
            if (id === undefined || isNaN(id)) {
                response.status(400).send('Wrong ID parameter!');
                return;
            }

            database.findById(id, (topic) => {
                response.status(200).json(topic);
            }, (error) => {
                response.status(500).send(error);
            });
        },
        create: (request, response) => {
            let topic = request.body;

            if (!topic) {
                response.status(400).send('Invalid topic to save!');
                return;
            }

            database.addTopic(new Topic(topic.title, topic.email), (newTopic) => {
                response.status(201).json(newTopic);
            }, (error) => {
                response.status(500).send(error);
            });
        }
    };

    apiRouter.get(apiRoot, handlers.all);
    apiRouter.get(apiRoot + '/:id', handlers.one);
    apiRouter.post(apiRoot, handlers.create);

    log.info('Topics API initialized.');
    module.exports = apiRouter;

}());
