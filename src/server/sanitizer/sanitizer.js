/**
 * @author  attilagyongyosi
 * @since   0.0.3
 *
 * @description
 * This module is a wrapper around the xss Node library,
 * providing convenience functions for sanitizing
 * various data types.
 */
(function () {
    'use strict';

    const xss = require('xss');

    let sanitizer = {};

    sanitizer.sanitizeString = function (string) {
        if (typeof string !== 'string') {
            return '';
        }

        return xss(string);
    };

    module.exports = sanitizer;

}());
